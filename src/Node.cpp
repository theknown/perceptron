#include "includes.h"


Node::Node(){
    font = al_load_font("font/Beholder.ttf",12,0);
}

Node::Node(int x_center,int y_center,int radius,float value,Type type): x_center(x_center), y_center(y_center),radius(radius), value(value), type(type){
    font = al_load_font("font/Beholder.ttf",12,0);
}


Node::Node(Node& node){
    this->x_center = node.x_center;
    this->y_center = node.y_center;
    this->radius = node.radius;
    font = al_load_font("font/Beholder.ttf",12,0); 
}



//same as the vertex and the node
void Node::setNode(Node node){
    this->x_center = node.x_center;
    this->y_center = node.y_center;
    this->radius = node.radius;
}



void Node::draw(){
    int x,y;
    // int color = value * 255;
    // switch(type){
    //     case INPUT:
    //         al_draw_circle(x_center,y_center,radius,al_map_rgb(color,color,color),2);
    //         break;
    //     case WEIGHT:
    //         al_draw_circle(x_center,y_center,radius,al_map_rgb(color,color,color),2);
    //         break;
    //     case OUTPUT:
    //         al_draw_circle(x_center,y_center,radius,al_map_rgb(color,color,color),2);
    //         break;
    // }
    if(value < 0){
         x = x_center -8 ;
         y = y_center -5 ;
    }

    else{
        x = x_center-5;
        y = y_center-5;
    }
    al_draw_textf(font,getColor(value,type), x,y,NULL,"%0.2f", value);
    al_draw_circle(x_center,y_center,radius,getColor(value,type),2);
}