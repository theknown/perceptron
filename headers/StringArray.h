#pragma once

#ifndef __STRINGARRAY__
#define __STRINGARRAY__
#include "includes.h"

class StringArray{
    std::vector<std::string> strings;
    int count;
public:
    StringArray(std::string line, char delimiter){
        std::string temp;
        count = 0;
        for(char a:line){
            if(a == delimiter)
            {
                strings.push_back(temp);
                temp = "";
                count++;
            }else{
                temp += a;
            }
        }
        strings.push_back(temp);
        count++;

    }

    double resultToDoubleNormalized(const char *a){
        if((*a) == 'p') return 1.0;
        if((*a) == 'e') return 0.0;
        else return 0.5;
    }


    double characterToDoubleNormalized(const char *a){
        if((*a) == '?') return 0.0;
        return ((int) (*a) - 97.0)/(26.0);
    }

    std::pair<std::vector<double>, double> getNormalizedData(){
        std::vector<double> inputParameters;
        inputParameters.reserve((this->count - 1) * sizeof(double));
        for(int i=1;i<count;i++){
            inputParameters.push_back(characterToDoubleNormalized(this->strings[i].c_str()));
        }

        return std::make_pair(inputParameters, resultToDoubleNormalized(this->strings[0].c_str()));
    }

    void logData(){
        std::ofstream outf("Datalog",std::ios::out|std::ios::app);
        if(outf.is_open()){
            outf<<std::endl<<"-----------Printing-----------"<<this->count<<'\n';
            for(int i=0;i<count;i++){
                outf<<this->strings[i]<<'\t';
            }
            outf<<std::endl<< "-----------End Printing-----------"<<'\n';
            outf.close();
        }
        else{
            std::cout<<"Unable to open file"<<std::endl;
        }

    }
    void display(){
        std::cout<<std::endl<<"-----------Printing-----------" <<this->count<<std::endl;
        for(int i=0;i<count;i++){
            std::cout<<this->strings[i]<<'\t';
}
        std::cout<<std::endl<<"-----------End Printing-----------"<<std::endl;
    }
    ~StringArray(){
    }
};


#endif