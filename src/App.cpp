#include "includes.h"
#include <stdlib.h>
App::App(){
    if(!al_init()) {
        exit(1);
    }

    //al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW); 
    al_set_new_display_flags(ALLEGRO_WINDOWED);
    al_set_new_display_flags(ALLEGRO_RESIZABLE);
    display = al_create_display(1366,768);
    const float fps =60;
    timer = al_create_timer(1.0/fps);
    event_queue = al_create_event_queue();
    al_init_font_addon();
    al_init_ttf_addon();
    al_install_mouse();
    al_register_event_source(event_queue,al_get_mouse_event_source());
    al_register_event_source(event_queue,al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_start_timer(timer);
    font = al_load_font("font/college.ttf",18,0);
    done = false;
        
    //objects.push_back(new Perceptron(22,0.01));
    perceptron = new Perceptron(22,0.01);

    learning1 = new Textbox(1150,160,1250, 200, "0.1");
    learning01 = new Textbox(1150,220,1250,260, "0.01");
    learning001 = new Textbox(1150, 280, 1250, 320, "0.001");

    train = new Textbox(1150,440,1250,480,"Train");
    test = new Textbox(1150,500,1250,540,"Test");

    //read data
    std::ifstream dataset("bin/mushrooms.csv");
    std::string line;
    std::getline(dataset,line);
    int count = 0;
    while(std::getline(dataset,line)){
        StringArray strings(line,',');
        if(++count > 7000){
            testData.push_back(strings.getNormalizedData());
            //strings.display();
            //strings.logData();
        }else{
            trainData.push_back(strings.getNormalizedData());
        }
    }


    // std::cout<< trainData.size() << std::endl;
    // std::cout<< testData.size() << std::endl;
} 

bool App::run(){
    int curTrainPos = 0;
    int curTestPos = 0;
    bool redraw = true;
    bool traintest = true;
    int trainLength = trainData.size();
    int testLength = testData.size();
    double error = 0;
    int count = 0;
    while(!done){
        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue,&ev);
        if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE){
            done = true;
        }
        else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN){
                pos_x = ev.mouse.x;
                pos_y = ev.mouse.y + 10;
                std::cout<<'\n' <<pos_y << '\t' <<pos_x << '\n';
            if(learning1->isClicked(pos_x,pos_y)){
                std::cout<< "Clicked 1\n" ;
                perceptron->setLearningRate(0.1);
            }

            if(learning01->isClicked(pos_x,pos_y)){
                std::cout<< "Clicked 2\n" ;
                perceptron->setLearningRate(0.01);
            }

            if(learning001->isClicked(pos_x,pos_y)){
                std::cout<< "Clicked 3\n" ;
                perceptron->setLearningRate(0.001);
            }
            redraw = true;

            if(train->isClicked(pos_x,pos_y)){
                std::cout<< "Clicked 4\n" ;
               // perceptron->train(trainData[curTrainPos++ % trainLength].first, {trainData[curTrainPos++ % trainLength].second});
                traintest = true; 
                // al_rest(2);          
            }
            redraw = true;

            if(test->isClicked(pos_x,pos_y)){
                std::cout<< "Clicked 5\n" ;
               // perceptron->test(testData[curTestPos++ % testLength].first);
                traintest = false;
                // al_rest(2);
            }
            redraw = true;

        }else if(ev.type == ALLEGRO_EVENT_TIMER){
            redraw = true;
        }

        if(redraw){
            redraw = false;
            al_clear_to_color(al_map_rgb(0,0,0));
            if (traintest == true){
                perceptron->train(trainData[curTrainPos++ % trainLength].first, {trainData[curTrainPos++ % trainLength].second});
            }
            else{
                count++;
                output =  perceptron->test(testData[curTestPos++ % testLength].first);
                if(output[0] != testData[curTestPos++ % testLength].second){
                    error += abs(output[0] - testData[curTestPos++ % testLength].second);
                 }
            al_draw_textf(font,al_map_rgb(200,200,200), 1100, 560, NULL, "Mean Error: %0.4f%",(error/count));
            }
            perceptron->draw();
            learning1->draw();
            learning01->draw();
            learning001->draw();
            al_draw_textf(font,al_map_rgb(200,200,200), 1145, 120, NULL, "Learning Rate %0.4f", perceptron->getLearningRate());
            train->draw();
            test->draw();
            al_draw_textf(font,al_map_rgb(200,200,200), 1130, 400, NULL, "Mode");
            al_flip_display();

            //error

            // for(int i=0; i<testLength;i++){
            //     if(testData[i]!= )
            // }

        }


    }
    // al_clear_to_color(al_map_rgb(0,0,0));
    // for(Object* iterator: objects){
    //     iterator->draw();
    // }
    // al_flip_display();
    // al_rest(5);
    // return true;    
}
