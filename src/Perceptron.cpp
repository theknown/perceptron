#include "includes.h"
#include <algorithm>
#include <iterator>

Perceptron::Perceptron(int num_inputs, double learning_rate): num_inputs(num_inputs), learning_rate(learning_rate){
    auto weightVector = getRandomVector<double>(num_inputs);
    double windowSize = 31;
    int x_init = 50;
    int vertex_length = 100;
    int node_radius = 20;
    int node_position = 150;


    //input layer initialization
    auto randomWeights = getRandomVector<double>(num_inputs);
    for(int i=0;i<num_inputs;i++){
        inputNodes.push_back(new Node(x_init + (45 * (i + 1)), node_position , node_radius , 0, INPUT));
        weightNodes.push_back(new Node(x_init + (45 * (i + 1)), (2*node_position) , node_radius , randomWeights[i], WEIGHT));
        inputVertex.push_back(new Vertex(x_init + (45 * (i + 1)),(node_position+node_radius),x_init + (45 * (i + 1)),(2*node_position-node_radius)));
        outputVertex.push_back(new Vertex(x_init + (45 * (i + 1)),(2*node_position+node_radius),600,(3*node_position-node_radius)));
    }

    thresholds = getRandomVector<double>(1);



    outputNode = new Node(600,(3*node_position), 20, 0,OUTPUT);
}




void Perceptron::train(std::vector<double> input, std::vector<double> target){
    //log weights to a file
    
    for(int i=0;i<num_inputs;i++){
        inputNodes[i]->setValue(input[i]);
    }
    
    
    double h = 0;
    for(int i=0;i<num_inputs;i++){
        h +=  input[i] * weightNodes[i]->getValue();
    }
    double output = h > thresholds[0] ? 1.0 : 0.0;
    for(int i=0;i<num_inputs;i++){
        float deltaWeight = learning_rate * (target[0] - output) * input[i];
        weightNodes[i]->deltaWeight(deltaWeight);
    }

    outputNode->setValue(output);

}

std::vector<double> Perceptron::test(std::vector<double> input){
    //log weights to a file
    
    for(int i=0;i<num_inputs;i++){
        inputNodes[i]->setValue(input[i]);
    }
    
    //  std::cout << "----------------------------------"<<std::endl;      
    //  std::copy(target.begin(), target.end(),
    //      std::ostream_iterator<double>(std::cout, " "));
    // std::cout << "----------------------------------"<<std::endl;
    double h = 0;
    for(int i=0;i<num_inputs;i++){
        h +=  input[i] * weightNodes[i]->getValue();
    }
    double output = h > thresholds[0] ? 1.0 : 0.0;

    outputNode->setValue(output);
    return {output};

}











void Perceptron::draw(){
    for(auto a: inputNodes)
        a->draw();
    for(auto a: weightNodes)
        a->draw();
    for(auto a: inputVertex)
        a->draw();
    for(auto a: outputVertex)
        a->draw();

    outputNode->draw();
    
}
