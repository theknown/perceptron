#pragma once

#ifndef __PERCEPTRON__
#define __PERCEPTRON__
#include"includes.h"

class Perceptron: public Object{
    std::vector<Node*> weightNodes;
    std::vector<Node*> inputNodes;
    std::vector<Vertex*> inputVertex;
    std::vector<Vertex*> outputVertex;
    std::vector<double> thresholds;
    Node* outputNode; 
    int num_inputs;
    double learning_rate;
public:
    Perceptron(int num_inputs, double learning_rate);
    void train(std::vector<double> input, std::vector<double> target);
    std::vector<double> test(std::vector<double> input);
    void draw();
    double getLearningRate(){
        return this->learning_rate;
    }
    void setLearningRate(double learning_rate){
        this->learning_rate = learning_rate;
    }
};


#endif
