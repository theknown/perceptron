#pragma once


#ifndef __NODE__
#define __NODE__
#include "includes.h"

class Node: public Object{
public:
    int x_center,y_center;
    int radius;
    float value;
    Type type;
    ALLEGRO_FONT *font;
public:
    Node();
    Node(int x_center,int y_center,int radius,float value,Type type);
    Node(Node& node);
    void setNode(Node node);
    virtual void draw();
    void setValue(double value){
        this->value = value;
    }
    double getValue(){
        return this->value;
    }

    void deltaWeight(double deltaWeight){
        this->value += deltaWeight;
    }
    
    ALLEGRO_COLOR getColor(double value, Type type){
        double r = 255;
        double g = 255;
        double b = 128;
       // value = 0 + (value + 1) / 2;  green 1 blue-1 red 0
        switch(type){
            case INPUT:                            //red green 0 1    
                if (value < 0.5){
                    r = 256 - 512 * value;
                    b = 0 + 512* value;
                    return al_map_rgb(r,0,b);
                }
                else if(value > 0.5){
                    g = 0 + 512 * (value - 0.5);
                    b = 256 - (512 * (value -0.5));
                    return al_map_rgb(0,g,b);
                }else{
                    return al_map_rgb(0,0,255);
                }
            case WEIGHT:
                if(value < 0)
                    return al_map_rgb(255,0,0);
                else if(value == 0)
                    return al_map_rgb(0,0,255);
                else 
                    return al_map_rgb(0,255,0);
            case OUTPUT:
                if(value == 0)
                    return al_map_rgb(r,0,0);
                else if(value == 1) 
                    return al_map_rgb(0,g,0);
                else
                    return al_map_rgb(255,255,255);
            default:
                return al_map_rgb(255,255,255);

        }
    }
};
#endif