#pragma once


#ifndef __TEXTBOX__
#define __TEXTBOX__
#include "includes.h"

class Textbox :public Object{
    int x_min,y_min;
    int x_max,y_max;
    int x1,y1,x2,y2;
    std::string text;
    int mode;
    ALLEGRO_FONT* font;

public:
    Textbox();
    Textbox(int x_min,int y_min,int x_max,int y_max,std::string text);
    Textbox(Textbox& textbox);
    void setTextbox(Textbox textbox);
    virtual void draw();
    bool isClicked(int mouse_x, int mouse_y){
        std::cout<<x_min << '\t' << x_max << '\t' << y_min << '\t' << y_max << '\n';
        if (mouse_x > x_min && mouse_x < x_max) {
            if(mouse_y > y_min && mouse_y < y_max) return true; 
        } 
        return false;
            
    }
};
#endif