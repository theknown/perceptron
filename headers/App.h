#pragma once


#ifndef __APP__
#define __APP__
#include"includes.h"

class App{
    int w,h;
    std::vector<std::pair<std::vector<double> , double> > trainData;
    std::vector<std::pair<std::vector<double> , double> > testData;
    std::vector<double> output;
    double partition = 0.8;
    ALLEGRO_MONITOR_INFO *info;
    ALLEGRO_DISPLAY *display;
    ALLEGRO_EVENT_QUEUE* event_queue;
    std::vector<Object*> objects;
    Textbox* learning1;
    Textbox* learning01;
    Textbox* learning001;
    Textbox* train;
    Textbox* test;
    Perceptron *perceptron;
    ALLEGRO_TIMER *timer;
    ALLEGRO_FONT* font;
    bool done;
    int pos_x,pos_y;

public:
    App();
    bool run();
    void readdata();
};
#endif