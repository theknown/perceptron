#include "includes.h"

    Vertex::Vertex(){}
    Vertex::Vertex(int x_initial,int y_initial,int x_final,int y_final): x_initial(x_initial),y_initial(y_initial),x_final(x_final),y_final(y_final){}
    Vertex::Vertex(Vertex& vertex){
        this->x_initial = vertex.x_initial;
        this->y_initial = vertex.y_initial;
        this->x_final = vertex.x_final;
        this->y_final = vertex.y_final;
    }


    //You can use the passing of the vertex to the constructor
    void Vertex::setVertex(Vertex vertex){
        this->x_initial = vertex.x_initial;
        this->y_initial = vertex.y_initial;
        this->x_final = vertex.x_final;
        this->y_final = vertex.y_final;
    }
    void Vertex::draw(){
        al_draw_line(x_initial,y_initial,x_final,y_final,al_map_rgb(255,255,255),2);
    }


