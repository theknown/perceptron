#pragma once

#ifndef __INCLUDES__
#define __INCLUDES__


#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include<allegro5/allegro_font.h>
#include<allegro5/allegro_ttf.h>

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <math.h>
#include "Object.h"
enum Type{
    INPUT, WEIGHT, OUTPUT
};
#include "Node.h"
#include "Vertex.h"
#include "Textbox.h"
#include "Perceptron.h"
#include "StringArray.h"
#include "App.h"


template<typename T>
std::vector<T> getRandomVector(int size){
    std::vector<T> temp;
    while(size --> 0){
        temp.push_back((T)rand()/ RAND_MAX);
    }
    return temp;
}


#endif