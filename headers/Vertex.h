#pragma once


#ifndef __VERTEX__
#define __VERTEX__
#include"includes.h"

class Vertex : public Object{
    int x_initial,y_initial;
    int x_final,y_final;
    int r,g,b;
public:
    Vertex();
    Vertex(int x_initial,int y_initial,int x_final,int y_final);
    Vertex(Vertex& vertex);
    void setVertex(Vertex vertex);
    void setColor(int r,int g,int b);
    virtual void draw();
};

#endif